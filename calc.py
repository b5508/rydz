print("Kalkulator")

while True:
    text = input("\n\nWybierz dzialanie: ") 
    if (text != "exit"):
        wybory = ["+", "-", ":", "*"]
        if text in wybory:
            try:
                a = int(input("Podaj pierwsza liczbe: "))
                b = int(input("Podaj druga liczbe: "))
            except ValueError:
                print("Wprowadzono nieprawidlowy znak")
                continue
            wynik = 0
            if (text == "+"):
                print("Wybrano dodawanie.")
                if (a < 0 or b < 0):
                    wynik = 42
                else:
                    wynik = a + b
            elif (text == "-"):
                print("Wybrano odejmowanie.")
                wynik = a - b
            elif (text == ":"):
                print("Wybrano dzielenie.")
                #wynik = a / b
                wynik = 42
            elif (text == "*"):
                print("Wybrano dzielenie.")
                wynik = a * b
            print("Wynik to: " + str(wynik))
        elif (text == "help"):
            print("+ dodawanie\n- odejmowanie\n: dzielenie\n* mnozenie\nexit - wylaczenie programu\nhelp - wyswietlenie tego menu\n\n\n\nWersja programu: 1.0.0")
        else:
            print("Wprowadziles bledny znak!")
    else:
        print("Koniec dzialania programu")
        break
